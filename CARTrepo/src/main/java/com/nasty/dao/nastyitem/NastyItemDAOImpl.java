package com.nasty.dao.nastyitem;

import com.nasty.dto.nastyitem.NastyItemDTO;
import com.nasty.entity.nastyitem.NastyItem;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by ian on 27/06/15.
 */

@NastyItemDAO
@Stateless
public class NastyItemDAOImpl implements Serializable, NastyItemDAOInterface {

    Logger logger = Logger.getLogger(NastyItemDAOImpl.class);

    @PersistenceContext(unitName="nasty")
    EntityManager em;

    @PostConstruct
    public void init() {
        logger.info("****************\n\n\n*****************\n\n\n**********");
    }

    @Override
    public Long insertItem(NastyItemDTO nastyItemDTO) {
        NastyItem nastyItem = new NastyItem();
        nastyItem.setItemName(nastyItemDTO.itemName);
        nastyItem.setItemDescription(nastyItemDTO.itemDescription);
        em.persist(nastyItem);
        return nastyItem.getId();
    }

    @Override
    public Collection<NastyItemDTO> getAllItems() {
        // TODO: convert to named query
        Collection<NastyItem> nastyItems = em.createQuery("from NastyItem n").getResultList();
        Collection<NastyItemDTO> blarg = nastyItems.parallelStream()
                .map(item -> convertToDTO(item)).collect(Collectors.toList());
        return blarg;
    }

    private NastyItemDTO convertToDTO(NastyItem nastyItem) {
        NastyItemDTO dto = new NastyItemDTO();
        dto.id = nastyItem.getId();
        dto.itemName = nastyItem.getItemName();
        dto.itemDescription = nastyItem.getItemDescription();
        return dto;
    }
}
