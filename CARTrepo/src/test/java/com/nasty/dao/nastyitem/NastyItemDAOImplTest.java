package com.nasty.dao.nastyitem;

import com.nasty.entity.nastyitem.NastyItem;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import javax.persistence.EntityManager;

import static org.testng.Assert.*;

import static org.mockito.Mockito.*;
/**
 * Created by ianrivers on 2015-06-28.
 */
public class NastyItemDAOImplTest {

    NastyItemDAOImpl nastyItemDAO;

    @Mock
    EntityManager entityManager;

    @BeforeTest
    public void setUp() {
        MockitoAnnotations.initMocks(this);


        nastyItemDAO = new NastyItemDAOImpl();
        nastyItemDAO.em = entityManager;
    }

   // @Test
    public void testAddItem() throws Exception {
        Long nastyItemId = nastyItemDAO.insertItem(null);
        // we must ensure the entity manager actually called persist (duh)
        verify(entityManager).persist(any());
        // this is secondary, not necessary
        Assert.assertNotNull(nastyItemId, "returned entity should not be null");
    }

    @Test
    public void testGetAllItems() throws Exception {

    }
}