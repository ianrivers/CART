package com.nasty.service.nastyitem;


import com.nasty.dao.nastyitem.NastyItemDAO;
import com.nasty.dto.nastyitem.NastyItemDTO;
import com.nasty.dao.nastyitem.NastyItemDAOInterface;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;

/**
 * Created by ian on 27/06/15.
 */

@NastyItemService
@Stateless
public class NastyItemServiceImpl implements NastyItemServiceInterface {


    @EJB @NastyItemDAO
    protected NastyItemDAOInterface nastyItemDAOInterface;


    @Override
    public Collection<NastyItemDTO> getItems() {
        return nastyItemDAOInterface.getAllItems();
    }

    @Override
    public Long insertItem(NastyItemDTO item) {
        return null;
    }

    @Override
    public NastyItemDTO getItem(Long id) {
        return null;
    }



}
