package com.nasty.service.nastyitem;

import com.nasty.dto.nastyitem.NastyItemDTO;

import java.util.Collection;

/**
 * Created by ian on 27/06/15.
 */
public interface NastyItemServiceInterface {

    public Collection<NastyItemDTO> getItems();

    public Long insertItem(NastyItemDTO item);

    public NastyItemDTO getItem(Long id);
}
