package com.nasty.dao.nastyitem;

import com.nasty.dto.nastyitem.NastyItemDTO;
import java.util.Collection;

/**
 * Created by ian on 27/06/15.
 */

public interface NastyItemDAOInterface {

    public Long insertItem(NastyItemDTO item);

    public Collection<NastyItemDTO> getAllItems();
}
