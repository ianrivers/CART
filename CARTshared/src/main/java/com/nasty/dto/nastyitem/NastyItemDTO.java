package com.nasty.dto.nastyitem;

/**
 * Created by ian on 27/06/15.
 */
public class NastyItemDTO {
    public Long id;
    public String itemName;
    public String itemDescription;
}
