package com.nasty.web.bean.nastyitem;

import com.nasty.dto.nastyitem.NastyItemDTO;
import com.nasty.service.nastyitem.NastyItemService;
import com.nasty.service.nastyitem.NastyItemServiceInterface;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 * Created by ian on 27/06/15.
 */
@RequestScoped
@ManagedBean(name="nastyItemJSFBean")
public class NastyItemJSFBean {

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

    public String hello = "hello";

    private String name;

    private String description;



    @EJB @NastyItemService
    NastyItemServiceInterface nastyItemServiceInterface;

    Logger logger = Logger.getLogger(NastyItemJSFBean.class);

    @PostConstruct
    public void init() {
        logger.info("****************\n\n\n*****************\n\n\n**********");
    }


    public void insertNastyItem() {
        nastyItemServiceInterface.insertItem(new NastyItemDTO());
    }

    public void getAllNastyItems() {
        nastyItemServiceInterface.getItems();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
